<div class="agendamentos form">
<?php echo $this->Form->create('Agendamento'); ?>
	<fieldset>
		<legend><?php echo __('Salvar Agendamento'); ?></legend>
	<?php
		echo $this->Form->input('medico_id',array('empty'=>'Selecione o médico.' ));
		echo $this->Form->input('paciente_id',array('empty'=>'Selecione o paciente.' ));
		echo $this->Form->input('data', array('type'=>'text', 'value'=> date('d-m-Y') ));
		echo $this->Form->input('id');


		echo '<div class="input text required mostra_horarios" style="border:1px;"><label for="AgendamentoData">Horários</label>';
			if(!empty($todos_os_horarios)){
				echo '<div style="width: 400px;height: 250px;overflow: scroll;">';
				echo 	'<table width="300">';
				
				foreach($todos_os_horarios as $hora_consulta => $horarios){

					echo '<tr>';
					echo '<td width="10"><input type="radio" name="data[Agendamento][hora]" value="'.$hora_consulta.'"'.$horarios['status'].' '.$horarios['checked'].'></td>';
					echo '<td width="390">'.$horarios['status_texto'].' '.$horarios['intervalo_consulta'].'</td>';
					echo '</tr>';

				}

				echo '</table>';
				echo '</div>';
			}else{

				echo 'Selecione os campos acima.';

			}
		echo '</div>';
	?>
	</fieldset>
<?php echo $this->Form->end(__('Salvar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Agendamentos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
