<div class="pacientes form">
<?php echo $this->Form->create('Paciente'); ?>
	<fieldset>
		<legend><?php echo __('Edit Paciente'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nome');
		echo $this->Form->input('data_de_nascimento');
		echo $this->Form->input('telefone');
		echo $this->Form->input('medico_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Paciente.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Paciente.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Agendamentos'), array('controller' => 'agendamentos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Agendamento'), array('controller' => 'agendamentos', 'action' => 'add')); ?> </li>
	</ul>
</div>
