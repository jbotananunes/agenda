<div class="pacientes view">
<h2><?php echo __('Paciente'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($paciente['Paciente']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nome'); ?></dt>
		<dd>
			<?php echo h($paciente['Paciente']['nome']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data De Nascimento'); ?></dt>
		<dd>
			<?php echo h($paciente['Paciente']['data_de_nascimento']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telefone'); ?></dt>
		<dd>
			<?php echo h($paciente['Paciente']['telefone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Medico'); ?></dt>
		<dd>
			<?php echo $this->Html->link($paciente['Medico']['id'], array('controller' => 'medicos', 'action' => 'view', $paciente['Medico']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Paciente'), array('action' => 'edit', $paciente['Paciente']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Paciente'), array('action' => 'delete', $paciente['Paciente']['id']), array(), __('Are you sure you want to delete # %s?', $paciente['Paciente']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Agendamentos'), array('controller' => 'agendamentos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Agendamento'), array('controller' => 'agendamentos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Agendamentos'); ?></h3>
	<?php if (!empty($paciente['Agendamento'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Medico Id'); ?></th>
		<th><?php echo __('Paciente Id'); ?></th>
		<th><?php echo __('Data'); ?></th>
		<th><?php echo __('Hora'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($paciente['Agendamento'] as $agendamento): ?>
		<tr>
			<td><?php echo $agendamento['id']; ?></td>
			<td><?php echo $agendamento['medico_id']; ?></td>
			<td><?php echo $agendamento['paciente_id']; ?></td>
			<td><?php echo $agendamento['data']; ?></td>
			<td><?php echo $agendamento['hora']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'agendamentos', 'action' => 'view', $agendamento['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'agendamentos', 'action' => 'edit', $agendamento['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'agendamentos', 'action' => 'delete', $agendamento['id']), array(), __('Are you sure you want to delete # %s?', $agendamento['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Agendamento'), array('controller' => 'agendamentos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
