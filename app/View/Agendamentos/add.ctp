<div class="agendamentos form">
<?php echo $this->Form->create('Agendamento',array("default"=>false)); ?>
	<fieldset>
		<legend><?php echo __('Add Agendamento'); ?></legend>
	<?php
		echo $this->Form->input('medico_id');
		echo $this->Form->input('paciente_id');
		echo $this->Form->input('data');
		echo $this->Form->input('hora',array('type'=>'select','options'=> $todos_os_horarios, 'empty'=> 'Selecione um horário') );
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Agendamentos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
