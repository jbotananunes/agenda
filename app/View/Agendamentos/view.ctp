<div class="agendamentos view">
<h2><?php echo __('Agendamento'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($agendamento['Agendamento']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Medico'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agendamento['Medico']['id'], array('controller' => 'medicos', 'action' => 'view', $agendamento['Medico']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Paciente'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agendamento['Paciente']['id'], array('controller' => 'pacientes', 'action' => 'view', $agendamento['Paciente']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data'); ?></dt>
		<dd>
			<?php echo h($agendamento['Agendamento']['data']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hora'); ?></dt>
		<dd>
			<?php echo h($agendamento['Agendamento']['hora']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Agendamento'), array('action' => 'edit', $agendamento['Agendamento']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Agendamento'), array('action' => 'delete', $agendamento['Agendamento']['id']), array(), __('Are you sure you want to delete # %s?', $agendamento['Agendamento']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Agendamentos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Agendamento'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
