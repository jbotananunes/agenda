<div class="agendamentos index">
	<h2><?php echo __('Agendamentos'); ?></h2>
	
	<?php echo $this->Form->create('AgendamentoBusca', array('id'=>'AgendamentoBusca','default' => false)); ?>
	<table cellpadding="0" cellspacing="0">
		<thead>
		<tr>
			<td style="width:300px;"> <?php echo $this->Form->input('data_inicial', array('label'=> 'Data Inicial','style'=> 'width:100px;float:left;','maxlength'=> 10 ) ); ?> 
			 <?php echo $this->Form->input('data_final', array('label'=> 'Data Final','style'=> 'width:100px;','maxlength'=> 10 ) ); ?> </td>
		<tr>
		<tr>
			<td> <?php echo $this->Form->input('medico_id', array('empty' => 'Selecione um médico.') ); ?> </td>
		<tr>
		<tr>
			<td> <?php echo $this->Form->input('paciente_id' , array('empty' => 'Selecione um paciente.')); ?> </td>
		<tr>
		</thead>
	</table>
<?php echo $this->Form->end(array('label'=>'Busca')); ?>

	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('medico_id'); ?></th>
			<th><?php echo $this->Paginator->sort('paciente_id'); ?></th>
			<th><?php echo $this->Paginator->sort('data'); ?></th>
			<th><?php echo $this->Paginator->sort('hora'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($agendamentos as $agendamento): ?>
	<tr>
		<td><?php echo h($agendamento['Agendamento']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($agendamento['Medico']['nome'], array('controller' => 'medicos', 'action' => 'edit', $agendamento['Medico']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($agendamento['Paciente']['nome'], array('controller' => 'pacientes', 'action' => 'edit', $agendamento['Paciente']['id'])); ?>
		</td>
		<td><?php echo h($agendamento['Agendamento']['data']); ?>&nbsp;</td>
		<td><?php echo h($agendamento['Agendamento']['hora']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $agendamento['Agendamento']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $agendamento['Agendamento']['id']), array(), __('Are you sure you want to delete # %s?', $agendamento['Agendamento']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Agendamento'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
