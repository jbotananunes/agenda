<div class="agendamentos form">
<?php echo $this->Form->create('Agendamento'); ?>
	<fieldset>
		<legend><?php echo __('Edit Agendamento'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('medico_id');
		echo $this->Form->input('paciente_id');
		echo $this->Form->input('data');
		echo $this->Form->input('hora');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Agendamento.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Agendamento.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Agendamentos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
