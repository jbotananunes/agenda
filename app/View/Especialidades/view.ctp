<div class="especialidades view">
<h2><?php echo __('Especialidade'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($especialidade['Especialidade']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Especialidade'); ?></dt>
		<dd>
			<?php echo h($especialidade['Especialidade']['especialidade']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Especialidade'), array('action' => 'edit', $especialidade['Especialidade']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Especialidade'), array('action' => 'delete', $especialidade['Especialidade']['id']), array(), __('Are you sure you want to delete # %s?', $especialidade['Especialidade']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Especialidades'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Especialidade'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('controller' => 'medicos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Medicos'); ?></h3>
	<?php if (!empty($especialidade['Medico'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nome'); ?></th>
		<th><?php echo __('Especialidade Id'); ?></th>
		<th><?php echo __('Crm'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($especialidade['Medico'] as $medico): ?>
		<tr>
			<td><?php echo $medico['id']; ?></td>
			<td><?php echo $medico['nome']; ?></td>
			<td><?php echo $medico['especialidade_id']; ?></td>
			<td><?php echo $medico['crm']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'medicos', 'action' => 'view', $medico['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'medicos', 'action' => 'edit', $medico['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'medicos', 'action' => 'delete', $medico['id']), array(), __('Are you sure you want to delete # %s?', $medico['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Medico'), array('controller' => 'medicos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
