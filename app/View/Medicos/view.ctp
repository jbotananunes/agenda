<div class="medicos view">
<h2><?php echo __('Medico'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($medico['Medico']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nome'); ?></dt>
		<dd>
			<?php echo h($medico['Medico']['nome']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Especialidade'); ?></dt>
		<dd>
			<?php echo $this->Html->link($medico['Especialidade']['id'], array('controller' => 'especialidades', 'action' => 'view', $medico['Especialidade']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Crm'); ?></dt>
		<dd>
			<?php echo h($medico['Medico']['crm']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Medico'), array('action' => 'edit', $medico['Medico']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Medico'), array('action' => 'delete', $medico['Medico']['id']), array(), __('Are you sure you want to delete # %s?', $medico['Medico']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Medicos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Medico'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Especialidades'), array('controller' => 'especialidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Especialidade'), array('controller' => 'especialidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Agendamentos'), array('controller' => 'agendamentos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Agendamento'), array('controller' => 'agendamentos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Agendamentos'); ?></h3>
	<?php if (!empty($medico['Agendamento'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Medico Id'); ?></th>
		<th><?php echo __('Paciente Id'); ?></th>
		<th><?php echo __('Data'); ?></th>
		<th><?php echo __('Hora'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($medico['Agendamento'] as $agendamento): ?>
		<tr>
			<td><?php echo $agendamento['id']; ?></td>
			<td><?php echo $agendamento['medico_id']; ?></td>
			<td><?php echo $agendamento['paciente_id']; ?></td>
			<td><?php echo $agendamento['data']; ?></td>
			<td><?php echo $agendamento['hora']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'agendamentos', 'action' => 'view', $agendamento['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'agendamentos', 'action' => 'edit', $agendamento['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'agendamentos', 'action' => 'delete', $agendamento['id']), array(), __('Are you sure you want to delete # %s?', $agendamento['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Agendamento'), array('controller' => 'agendamentos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Pacientes'); ?></h3>
	<?php if (!empty($medico['Paciente'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nome'); ?></th>
		<th><?php echo __('Data De Nascimento'); ?></th>
		<th><?php echo __('Telefone'); ?></th>
		<th><?php echo __('Medico Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($medico['Paciente'] as $paciente): ?>
		<tr>
			<td><?php echo $paciente['id']; ?></td>
			<td><?php echo $paciente['nome']; ?></td>
			<td><?php echo $paciente['data_de_nascimento']; ?></td>
			<td><?php echo $paciente['telefone']; ?></td>
			<td><?php echo $paciente['medico_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'pacientes', 'action' => 'view', $paciente['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'pacientes', 'action' => 'edit', $paciente['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'pacientes', 'action' => 'delete', $paciente['id']), array(), __('Are you sure you want to delete # %s?', $paciente['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
