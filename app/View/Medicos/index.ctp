<div class="medicos index">
	<h2><?php echo __('Medicos'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nome'); ?></th>
			<th><?php echo $this->Paginator->sort('especialidade_id'); ?></th>
			<th><?php echo $this->Paginator->sort('crm'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($medicos as $medico): ?>
	<tr>
		<td><?php echo h($medico['Medico']['id']); ?>&nbsp;</td>
		<td><?php echo h($medico['Medico']['nome']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($medico['Especialidade']['id'], array('controller' => 'especialidades', 'action' => 'view', $medico['Especialidade']['id'])); ?>
		</td>
		<td><?php echo h($medico['Medico']['crm']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $medico['Medico']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $medico['Medico']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $medico['Medico']['id']), array(), __('Are you sure you want to delete # %s?', $medico['Medico']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Medico'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Especialidades'), array('controller' => 'especialidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Especialidade'), array('controller' => 'especialidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Agendamentos'), array('controller' => 'agendamentos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Agendamento'), array('controller' => 'agendamentos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pacientes'), array('controller' => 'pacientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paciente'), array('controller' => 'pacientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
