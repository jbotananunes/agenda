<?php
App::uses('AppModel', 'Model');
/**
 * Agendamento Model
 *
 * @property Medico $Medico
 * @property Paciente $Paciente
 */
class Agendamento extends AppModel {


public function beforeValidate(array $options = array()) {
    
    if (!empty($this->data['Agendamento']['data'])) {

        $this->data['Agendamento']['data'] = $this->dateFormatBeforeSave($this->data['Agendamento']['data']);
        
    }
    
    return true;

}

public function dateFormatBeforeSave($dateString) {
    return date('Y-m-d', strtotime($dateString));
}	

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'medico_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Selecione um médico',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'paciente_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Selecione um paciente.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'data' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'O campo é tipo data.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'O Data campo está vazio.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'hora' => array(
			'time' => array(
				'rule' => array('time'),
				'message' => 'O campo é do tipo hora.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo hora está vazio.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Medico' => array(
			'className' => 'Medico',
			'foreignKey' => 'medico_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Paciente' => array(
			'className' => 'Paciente',
			'foreignKey' => 'paciente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
