<?php

class HorariosAgendamento extends AppModel {
    	
    	public $useTable = false;


		public function somaTempo($tempo_somado, $hora, $tipo_formatacao_entrada, $tipo_formatacao_saida){

					$proximaHora = DateTime::createFromFormat($tipo_formatacao_entrada, $hora); 
					$proximaHora->add(new DateInterval($tempo_somado));             
					$proximaHora = $proximaHora->format($tipo_formatacao_saida);

					return $proximaHora;

		}


    	public function agendaMedicoDia($todos_os_horarios = '', $agendamento_medico = '', $consulta_hora = ''){



				$horario_agendamento_medico = Hash::extract($agendamento_medico, '{n}.Agendamento.hora');
				$horarios_dia_array = array();


	

				foreach ($todos_os_horarios as $hora_consulta => $intervalo_consulta) {
					
					$horarios = $hora_consulta . ':00';
					$horarios_dia_array[$hora_consulta]['intervalo_consulta'] = $intervalo_consulta;

					if(in_array($horarios,$horario_agendamento_medico)){

						$horarios_dia_array[$hora_consulta]['status'] = 'disabled';
						$horarios_dia_array[$hora_consulta]['status_texto'] = 'Marcado';


						
					}else{

						$horarios_dia_array[$hora_consulta]['status'] = '';
						$horarios_dia_array[$hora_consulta]['status_texto'] = 'Disponível';
						

					}

					 	if($horarios == $consulta_hora){

						 	$horarios_dia_array[$hora_consulta]['checked'] = 'checked';
						 	$horarios_dia_array[$hora_consulta]['status'] = '';

						 }else{

						 	$horarios_dia_array[$hora_consulta]['checked'] = '';

						 }
						

					
				}

				return $horarios_dia_array; 


    	}


    	public function horariosAgenda(){

					$intervalo = 20;
			    	$arrayIntervaloDeHoras = array();

			    	for($i=0;$i<=23;$i++){

			    		if($i == 0){

			    			$arrayIntervaloDeHoras['00:00'] = '00:00 até '.'00:'.$intervalo;
			    			$arrayIntervaloDeHoras['00:'.$intervalo] = '00:'.$intervalo.' até '.'00:'.($intervalo+20);
			    			$arrayIntervaloDeHoras['00:'.($intervalo+20)] = '00:'.($intervalo+20).' até '.$this->somaTempo('PT20M','00:'.($intervalo+20),'H:i','H:i');

			    			
			    		}else if($i > 0 && $i <= 9){

			    			$arrayIntervaloDeHoras['0'. $i.':00'] = '0'. $i.':00 até '.'0'. $i .':' .$intervalo;
			    			$arrayIntervaloDeHoras['0'. $i .':' .$intervalo] = '0'. $i .':' .$intervalo.' até '.'0'. $i .':' .($intervalo+20);
			    			$arrayIntervaloDeHoras['0'. $i .':' .($intervalo+20)] = '0'. $i .':' .($intervalo+20).' até '.$this->somaTempo('PT20M','0'. $i .':' .($intervalo+20),'H:i','H:i');

			    		}else{

			    			$arrayIntervaloDeHoras[$i .':' .'00'] = $i .':00 até '.$i .':' .$intervalo; 
			    			$arrayIntervaloDeHoras[$i .':' .$intervalo] = $i .':' .$intervalo.' até '.$i .':' .($intervalo+20);
			    			$arrayIntervaloDeHoras[$i .':' .($intervalo+20)] =  $i .':' .($intervalo+20).' até '.$this->somaTempo('PT20M',$i .':' .($intervalo+20),'H:i','H:i');


			    		}

			    	}


			    	return $arrayIntervaloDeHoras;


    }

}

?>