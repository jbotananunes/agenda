<?php
App::uses('AppController', 'Controller');
/**
 * Agendamentos Controller
 *
 * @property Agendamento $Agendamento
 * @property PaginatorComponent $Paginator
 */
class AgendamentosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	
	 public $components = array('RequestHandler', 'Paginator'); 
 	public $helpers = array('Js' => array('Jquery'), 'Paginator'); 

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Agendamento->recursive = 0;
		$this->set('agendamentos', $this->Paginator->paginate());

		$medicos = $this->Agendamento->Medico->find('list',array('fields'=> array('id','nome' )));
		$pacientes = $this->Agendamento->Paciente->find('list',array('fields'=> array('id','nome' )));

		$this->set(compact('medicos', 'pacientes' ));
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Agendamento->exists($id)) {
			throw new NotFoundException(__('Invalid agendamento'));
		}
		$options = array('conditions' => array('Agendamento.' . $this->Agendamento->primaryKey => $id));
		$this->set('agendamento', $this->Agendamento->find('first', $options));
	}

	public function busca() {

		   $condicao = array();


       if ($this->request->is('post')) {    
    	
    		if($this->request->data['Agendamento']['medico_id']){

    			$condicao = array('Agendamento.medico_id' => $this->request->data['Agendamento']['medico_id']);

    		}
		   
	   }
	   
	   $this->paginate = array(
        	'limit' => 50,
        	'conditions' => $condicao,
        	// 'order' => array(
        	//     'OrdemDeServico.modified' => 'DESC'
        	// )
    	);

	   
		
    	$this->set('agendamentos', $this->paginate());
		$this->set('condicao', $condicao);

	}

	public function mostrar_horarios() {

		if ($this->request->is('ajax')) {
    		$this->layout = "ajax";
		}

		
		$this->loadModel('HorariosAgendamento');
		$todos_os_horarios = $this->HorariosAgendamento->horariosAgenda();

		$options = array('conditions' => array('Agendamento.medico_id'  => $this->request->data['Agendamento']['medico_id'], 'Agendamento.data'  => date('Y-m-d', strtotime($this->request->data['Agendamento']['data'])) ));
		$agendamentos_medico  = $this->Agendamento->find('all', $options);

		// echo date('Y-m-d', strtotime($this->request->data['Agendamento']['data'])).' '.$this->request->data['Agendamento']['medico_id'];
		
		// echo '<pre>';
		// print_r($agendamentos_medico);

		$agenda_hora_agendamento = $this->Agendamento->find('first', array(
		    	'fields'=> 'hora',
        		'conditions' => array('Agendamento.id' => $this->request->data['Agendamento']['id'])
    	));


		$this->loadModel('HorariosAgendamento');
		$todos_os_horarios = $this->HorariosAgendamento->agendaMedicoDia($todos_os_horarios, $agendamentos_medico, $agenda_hora_agendamento['Agendamento']['hora']);

		$this->set(compact('todos_os_horarios'));
		
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Agendamento->create();
			if ($this->Agendamento->save($this->request->data)) {
				$this->Session->setFlash(__('O agendamento foi salvo.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O agendamento não pode ser salvo. Por favor, tente novamente.'));
			}
		}
		$medicos = $this->Agendamento->Medico->find('list',array('fields'=> array('id','nome' )));
		$pacientes = $this->Agendamento->Paciente->find('list',array('fields'=> array('id','nome' )));

		$this->loadModel('HorariosAgendamento');
		$todos_os_horarios = $this->HorariosAgendamento->horariosAgenda();



		$options = array('conditions' => array('Agendamento.medico_id'  => $this->request->data['Medico']['id'], 'Agendamento.data'  => date("Y-m-d") ));
		$agendamentos_medico  = $this->Agendamento->find('all', $options);

		

		$this->loadModel('HorariosAgendamento');
		$todos_os_horarios = array();
	
		$this->set(compact('medicos', 'pacientes', 'todos_os_horarios','agendamentos_medico' ));

		$this->render('/Elements/agendamentos/add');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Agendamento->exists($id)) {
			throw new NotFoundException(__('Invalid agendamento'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Agendamento->save($this->request->data)) {
				$this->Session->setFlash(__('O agendamento foi salvo.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O agendamento não pode ser salvo. Por favor, tente novamente.'));
			}
		} else {
			$options = array('conditions' => array('Agendamento.' . $this->Agendamento->primaryKey => $id));
			$this->request->data = $this->Agendamento->find('first', $options);



		}
		
		$medicos = $this->Agendamento->Medico->find('list',array('fields'=> array('id','nome' )));
		$pacientes = $this->Agendamento->Paciente->find('list',array('fields'=> array('id','nome' )));
		
		$this->loadModel('HorariosAgendamento');
		$todos_os_horarios = $this->HorariosAgendamento->horariosAgenda();

		$options = array('conditions' => array('Agendamento.medico_id'  => $this->data['Agendamento']['medico_id'], 'Agendamento.data'  => date("Y-m-d") ));
		$agendamento_medico  = $this->Agendamento->find('all', $options);


		$this->loadModel('HorariosAgendamento');
		$todos_os_horarios = $this->HorariosAgendamento->agendaMedicoDia($todos_os_horarios, $agendamento_medico,$this->request->data['Agendamento']['hora']);
	
		$this->set(compact('medicos', 'pacientes', 'todos_os_horarios'));
		$this->render('/Elements/agendamentos/add');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Agendamento->id = $id;
		if (!$this->Agendamento->exists()) {
			throw new NotFoundException(__('Invalid agendamento'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Agendamento->delete()) {
			$this->Session->setFlash(__('The agendamento has been deleted.'));
		} else {
			$this->Session->setFlash(__('The agendamento could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
