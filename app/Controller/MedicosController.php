<?php
App::uses('AppController', 'Controller');
/**
 * Medicos Controller
 *
 * @property Medico $Medico
 * @property PaginatorComponent $Paginator
 */
class MedicosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Medico->recursive = 1;
		$this->set('medicos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Medico->exists($id)) {
			throw new NotFoundException(__('Invalid medico'));
		}
		$options = array('conditions' => array('Medico.' . $this->Medico->primaryKey => $id));
		$this->set('medico', $this->Medico->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Medico->create();
			if ($this->Medico->save($this->request->data)) {
				$this->Session->setFlash(__('The medico has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The medico could not be saved. Please, try again.'));
			}
		}
		$especialidades = $this->Medico->Especialidade->find('list', array('fields'=> array('id', 'especialidade') ) );

		
		$this->set(compact('especialidades'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Medico->exists($id)) {
			throw new NotFoundException(__('Invalid medico'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Medico->save($this->request->data)) {
				$this->Session->setFlash(__('The medico has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The medico could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Medico.' . $this->Medico->primaryKey => $id));
			$this->request->data = $this->Medico->find('first', $options);
		}
		$especialidades = $this->Medico->Especialidade->find('list');
		$this->set(compact('especialidades'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Medico->id = $id;
		if (!$this->Medico->exists()) {
			throw new NotFoundException(__('Invalid medico'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Medico->delete()) {
			$this->Session->setFlash(__('The medico has been deleted.'));
		} else {
			$this->Session->setFlash(__('The medico could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
